//
//  ChatsViewController.swift
//  SkrumbleSDK_Example
//
//  Created by Gabriel Hernández Ontiveros on 2018-07-06.
//  Copyright © 2018 CocoaPods. All rights reserved.
//

import UIKit
import SkrumbleSDK

class ChatsViewController: UITableViewController {

    var me: User!
    var team: Team!
    var chats: [Chat] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let hud = MBProgressHUD.showAdded(to: self.view, animated: true)
        hud.label.text = "Fetching chats..."
        
        SKApi.Chats.getChats(teamId: self.team.id).request { [weak hud, weak self] (response) in
            hud?.hide(animated: true)
            self?.chats = response?.array ?? []
            self?.tableView.reloadData()
            
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return chats.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "chat_cell", for: indexPath)

        let chat = chats[indexPath.row]
        cell.textLabel?.text = chat.getChatName(me)
        cell.detailTextLabel?.text = chat.type.rawValue == 0 ? "private" : "room"

        return cell
    }
 

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let chat = chats[indexPath.row]
        self.performSegue(withIdentifier: "chat_segue", sender: chat)
    }

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? ChatViewController, let chat = sender as? Chat {
            vc.chat = chat
            vc.me = me
        }
    }
 

}
