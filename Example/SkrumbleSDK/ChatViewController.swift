//
//  ChatViewController.swift
//  SkrumbleSDK_Example
//
//  Created by Gabriel Hernández Ontiveros on 2018-07-06.
//  Copyright © 2018 CocoaPods. All rights reserved.
//

import UIKit
import SkrumbleSDK

class ChatViewController: UITableViewController {
    
    var chat: Chat!
    var me: User!
    var messages: [Message] = []
    
    @IBOutlet weak var sendMessageTextView: UITextField!
    @IBOutlet weak var sendButton: UIButton!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.estimatedRowHeight = 75.0
        tableView.rowHeight = UITableView.automaticDimension
        
        let hud = MBProgressHUD.showAdded(to: self.view, animated: true)
        hud.label.text = "Loading chat..."
        SKApi.Messages.messages(chatId: chat.id).request { [weak self, weak hud] (response) in
            hud?.hide(animated: true)
            if let messages = response?.array {
                self?.messages = messages.sorted(by: { (m1, m2) -> Bool in
                    guard let d1 = m1.createdAt else { return false }
                    guard let d2 = m2.createdAt else { return false }
                    return d1 < d2
                })
            }
            self?.tableView.reloadData()
            self?.scrollToBottom()
        }
        
        SKApi.addChatSocketObserverDelegate(self)
    }
    
    deinit {
        SKApi.removeChatSocketObserverDelegate(self)
    }
    
    func scrollToBottom() {
        
        DispatchQueue.main.async {
            self.tableView.scrollRectToVisible(self.tableView.tableFooterView!.frame, animated: true)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messages.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "message_cell", for: indexPath)

        if let cell = cell as? MessageCell {
            let message = messages[indexPath.row]
            
            cell.setup(with: message)
        }

        return cell
    }
    
    @IBAction func sendMessage(_ sender: Any) {
        let newMessage = self.sendMessageTextView.text
        guard let text = newMessage else { return }
        
        let hud = MBProgressHUD.showAdded(to: self.view, animated: true)
        SKApi.Messages.sendText(body: text, chatId: chat.id).request { [weak self, weak hud] (response) in
            hud?.hide(animated: true)
            
            if let message = response?.value {
                self?.sendMessageTextView.text = ""
                message.from = self?.me
                self?.newMessage(message)
            } else {
                // error
            }
        }
    }
    
    func newMessage(_ message: Message) {
        tableView.beginUpdates()
        messages.append(message)
        
        let indexPath = IndexPath(row: messages.count - 1, section: 0)
        tableView.insertRows(at: [indexPath], with: .automatic)
        tableView.endUpdates()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.scrollToBottom()
        }
    }
}

extension ChatViewController: ChatSocketObserverDelegate {
    func incomingMessage(_ message: Message, chatId: String) {
        if chatId != chat.id { return }
        
        newMessage(message)
    }
}
