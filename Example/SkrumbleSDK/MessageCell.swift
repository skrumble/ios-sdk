//
//  MessageCell.swift
//  SkrumbleSDK_Example
//
//  Created by Gabriel Hernández Ontiveros on 2018-07-06.
//  Copyright © 2018 CocoaPods. All rights reserved.
//

import UIKit
import SkrumbleSDK

class MessageCell: UITableViewCell {
    
    lazy var dateFormatter = DateFormatter()

    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var senderLabel: UILabel!
    @IBOutlet weak var messageBodyLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm"
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        dateLabel.text = ""
        senderLabel.text = ""
        messageBodyLabel.text = ""
    }
    
    func setup(with message: Message) {
        var dateString = "a few seconds ago"
        
        if let date = message.createdAt {
            dateString = dateFormatter.string(from: date)
        }
        
        dateLabel.text = dateString
        senderLabel.text = message.from?.firstName ?? "Someone"
        
        let messageType = message.type ?? .text
        switch messageType {
        
        case .text:
            messageBodyLabel.text = message.body
            break
        
        default:
            messageBodyLabel.text = message.type?.rawValue ?? "undefined"
            break
        }
        
    }
}
