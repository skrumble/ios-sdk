//
//  MeViewController.swift
//  SkrumbleSDK_Example
//
//  Created by Gabriel Hernández Ontiveros on 2018-07-06.
//  Copyright © 2018 CocoaPods. All rights reserved.
//

import UIKit
import SkrumbleSDK

class MeViewController: UITableViewController {
    
    var me: User!

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        nameLabel.text = me.firstName
        emailLabel.text = me.email
        
    }

    @IBAction func logout(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 1 {
            let team = self.me.teams[indexPath.row]
            self.performSegue(withIdentifier: "team_info", sender: team)
        }
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return super.numberOfSections(in: tableView)
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 1 {
            return self.me.teams.count
        }
        return super.tableView(tableView, numberOfRowsInSection: section)
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 1 {
            
            let teamIdentifier = "team_cell"
            let cell: UITableViewCell
            if let c = tableView.dequeueReusableCell(withIdentifier: teamIdentifier) {
                cell = c
            } else {
                cell = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: teamIdentifier)
                cell.accessoryType = .disclosureIndicator
            }
            
            let team = me.teams[indexPath.row]
            cell.textLabel?.text = team.name
            return cell
        }
        
        return super.tableView(tableView, cellForRowAt: indexPath)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? TeamInfoViewController, let team = sender as? Team {
            vc.team = team
            vc.me = self.me
        }
    }
}
