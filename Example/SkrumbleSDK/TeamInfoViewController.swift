//
//  TeamInfoViewController.swift
//  SkrumbleSDK_Example
//
//  Created by Gabriel Hernández Ontiveros on 2018-07-06.
//  Copyright © 2018 CocoaPods. All rights reserved.
//

import UIKit
import SkrumbleSDK

class TeamInfoViewController: UITableViewController {

    var me: User!
    var team: Team!
    
    @IBOutlet weak var teamNameLabel: UILabel!
    @IBOutlet weak var positionLabel: UILabel!
    @IBOutlet weak var homeNumberLabel: UILabel!
    @IBOutlet weak var workNumberLabel: UILabel!
    @IBOutlet weak var extensionLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.teamNameLabel.text = team.name
        self.positionLabel.text = team.position ?? "-"
        self.homeNumberLabel.text = team.homeNumber ?? "-"
        self.workNumberLabel.text = team.workNumber ?? "-"
        self.extensionLabel.text = team.teamExtension.joined(separator: ", ")
 
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? ChatsViewController {
            vc.team = self.team
            vc.me = self.me
        }
    }

}
