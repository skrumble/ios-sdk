//
//  NSPointerArray+Utils.swift
//  SkrumbleSDK
//
//  Created by Gabriel Hernández on 5/27/18.
//

import Foundation

extension NSPointerArray {
    
    func add(_ object: AnyObject?) {
        
        guard let strongObject = object else { return }
        
        let pointer = Unmanaged.passUnretained(strongObject).toOpaque()
        addPointer(pointer)
        
    }
    
    func object(at index: Int) -> AnyObject? {
        guard index < count, let pointer = self.pointer(at: index) else { return nil }
        return Unmanaged<AnyObject>.fromOpaque(pointer).takeUnretainedValue()
    }
    
    func remove(at index: Int) {
        guard index < count else { return }
        removePointer(at: index)
    }
    
    func remove(_ object: AnyObject) {
        let index = self.index(of: object)
        remove(at: index)
        
    }
    
    func clean() {
        while self.count > 0 {
            self.removePointer(at: 0)
        }
    }
    
    func index(of object: AnyObject) -> Int {
        var index = NSNotFound
        
        for i in 0..<self.count {
            if object.isEqual(self.pointer(at: i)) {
                index = i
                break
            }
        }
        return index
    }
    
    func forLoop(_ completion: ((Any?) -> Void)?) {
        if completion == nil {
            return
        }
        
        for i in 0..<self.count {
            if let object = self.object(at: i) {
                completion?(object)
            }
        }
    }
}
