//
//  String+Utils.swift
//  Pods
//
//  Created by Gabriel Hernández on 6/23/18.
//

import Foundation

extension String {
    func appendingPathComponent(path: String) -> String {
        var newString = self
        if !self.hasSuffix("/") {
            newString = newString + "/"
        }
        
        var aPath = path
        if aPath.hasPrefix("/") {
            aPath.remove(at: aPath.startIndex)
        }
        newString = newString + aPath
        return newString
    }
}
