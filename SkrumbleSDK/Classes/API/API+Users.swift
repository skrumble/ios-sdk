//
//  API+Users.swift
//  SKWebRTC_Example
//
//  Created by Gabriel Hernández on 5/23/18.
//  Copyright © 2018 CocoaPods. All rights reserved.
//

import Foundation
import SwiftyJSON

extension SKApi {
    public enum Users : APIEndpoint {
        case me()
        case teamMembers(teamId : String)
        
        
        
        public func fillResponse(_ response: inout APIResponse<User>) {
            switch self {
            case .me:
                guard let json = response.json else { break }
                let me = User(with: json)
                response.value = me
                break
            case .teamMembers:
                guard let json = response.json else {break}
                var teamMembers : [User] = []
                let arrJson = json.arrayValue
                for teamMemberJson in arrJson {
                    let teamMember = User(with: teamMemberJson)
                    teamMembers.append(teamMember)
                }
                response.array = teamMembers
                break
            }
        }
        
        public typealias APIResponseType = APIResponse<User>
        
        public func request(_ completion: ((APIResponse<User>?) -> Void)?) {
            switch self {
//            case .updateAvatar(_, let image):
//                _uploadImage(image, fileName: "profile_picture.png",  completion: completion)
//                break
            default:
                _sendRequest(completion: completion)
                break
            }
        }
        
        public var endpoint: String {
            switch self {
            case .me:
                return "/user/me"
            case .teamMembers(let teamId):
                return "/team/" + teamId + "/users"
            }
        }
        
        public func parameters() -> [String: Any]? {
            var params: [String : Any] = [:]
            switch self {
            case .me:
                params["populate"] = "teams"
                params["extension"] = "true"
                break
            case .teamMembers:
                break
            }
            return params
        }
        
        public func method() -> String {
            switch self {
            case .me:
                return "GET"
            case .teamMembers:
                return "GET"
            }
        }
        
        
    }
}
