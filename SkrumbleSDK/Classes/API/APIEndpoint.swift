//
//  API.swift
//  SKWebRTC_Example
//
//  Created by Gabriel Hernández on 2/22/18.
//  Copyright © 2018 Skrumble Technologies Inc. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON


private var manager: SessionManager = {
    
    let configuration = URLSessionConfiguration.default
    configuration.requestCachePolicy = .reloadIgnoringLocalCacheData
    configuration.urlCache = nil
    return SessionManager(configuration: configuration)
}()

//MARK: - Endpoint Protocol
public protocol APIEndpoint {
    
    associatedtype APIResponseType: APIResponseProtocol
    
    var endpoint: String { get }
    var authToken: String? { get }
    func parameters() -> [String: Any]?
    func method() -> String
    
    /// Purpose of this method is switch between sending an api and uploading
    /// a file
    ///
    /// - Parameter completion: callback
    func request(_ completion: ((APIResponseType?) -> Void)?)
    var sendToSocket: Bool { get }
    func didSucceed(_ statusCode: StatusCode) -> Bool
    func fillResponse(_ response: inout APIResponseType)

    func URLString() -> String

    typealias APICallback = (APIResponseType?) -> Void

}

extension APIEndpoint {

    var headers: [String: String] {
        var _headers: HTTPHeaders = [:]
        if let authToken = authToken {
            _headers["Authorization"] = authToken
        }
        return _headers
    }
    
    public var authToken: String? {
        if self is SKApi.Auth {
            return nil
        }
        
        if let accessToken = SKApi.accessToken {
            return accessToken.tokenType + " " + accessToken.accessToken
        }
        
        return nil
    }
    
    public var sendToSocket: Bool {
        return false
    }
    
    func request(_ completion: APICallback?) {
        _sendRequest(completion: completion)
    }
    func requestHttp(_ completion: APICallback?) {
        _sendRequest(forceHttp: true, completion: completion)
    }
    
    public func URLString() -> String {
        
        var rootUrl = ""
        if self is SKApi.Auth, let apiHostname = SKApi.apiConfig?.authHostname {
            rootUrl = "\(apiHostname)"
        } else if let apiHostname = SKApi.apiConfig?.apiHostname {
            rootUrl = "\(apiHostname)"
        } else {
            // TODO gabriel print warning
        }
        let url = rootUrl.appendingPathComponent(path: endpoint)
        return url
    }
    
    public func didSucceed(_ statusCode: StatusCode) -> Bool {
        return statusCode.success()
    }
    
    // end default impl
    
    internal func _sendRequest(_ attempts: Int = 0, forceHttp: Bool = false, completion: APICallback? = nil) {
        
        let _method = method()
        let _urlString = URLString()
        let _parameters = parameters()
        let _headers: HTTPHeaders = headers
        
//        NULog.logApiSending(requestDescription)
        
        //TODO: Arnaud - no need to wait for the socket to connect, just check and do http if no socket connected
        
        if sendToSocket && !forceHttp {
            
            if !SocketManager.isConnectedAndRegistered() {
                SocketManager.connectSocket()
                SKApi.queueApiCall(self, callback: completion)
                return
            }
            
            if SKApi.refreshingToken {
                SKApi.queueApiCall(self, callback: completion)
                return
            }
            
            let request = NSMutableURLRequest(url: URL(string: _urlString)!)
            request.httpMethod = _method
            request.allHTTPHeaderFields = _headers
            
            if let params = _parameters {
                do {
                    let httpBody: Data = try JSONSerialization.data(withJSONObject: params, options: .prettyPrinted)
                    request.httpBody = httpBody
                } catch {
                    // TODO
                }
            }
            
            let socketCallback: Socket.CallBackSocketRequest = { (response, status) in
                var json: JSON?
                let statusCode = StatusCode(rawValue: status) ?? StatusCode.none
                if let jsonResponse = response {
                    json = JSON(jsonResponse)
                }
                self.handleResponse(attempts, response: json, statusCode: statusCode, completion: completion)
            }
            SocketManager.getSharedInstance().sendApiRequest(urlRequest: request, timeout: 0, callback: socketCallback)
        } else {
            let httpMethod = HTTPMethod(rawValue: _method)
            manager.request(_urlString, method: httpMethod ?? HTTPMethod.get, parameters: _parameters, headers: _headers).responseJSON {
                
                var json: JSON?
                if let responseVal = $0.result.value {
                    json = JSON(responseVal)
                }
                var statusCode: StatusCode = .none
                if let httpResponse = $0.response, let status = StatusCode(rawValue: httpResponse.statusCode) {
                    statusCode = status
                }
                self.handleResponse(attempts, response: json, statusCode: statusCode, completion: completion)
            }
        }
    }
    
    internal func _uploadImage(_ image: UIImage, fileName: String, completion: APICallback? = nil) {
        guard let imgData = image.jpegData(compressionQuality: 1.0) else {
            // TODO gabriel callback
            return
        }
        
        _uploadFile(imgData, fileName: fileName, completion: completion)
        
    }
    
    internal func _uploadFile(_ skrumbleFile: SkrumbleFile, completion: APICallback? = nil) {
        guard let data = skrumbleFile.getData() else {
            // TODO arnaud callback
            return;
        }
        _uploadFile(data, fileName: skrumbleFile.fileName, completion: completion)
    }
    
    internal func _uploadFile(_ file: Data, fileName: String, completion: APICallback? = nil) {
        
        
        let _method = method()
        let httpMethod: HTTPMethod = HTTPMethod(rawValue: _method)!
        let _urlString = URLString()
        let params = parameters() ?? [:]
        
        print("Starting image upload!!")
        
        let _headers: HTTPHeaders = self.headers
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in params {
                if let value = value as? String {
                    multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
                }
            }
            multipartFormData.append(file, withName: "file", fileName: fileName, mimeType: "")
            
        }, usingThreshold: SessionManager.multipartFormDataEncodingMemoryThreshold, to:_urlString, method: httpMethod, headers: _headers) { (encodingResult) in
            switch encodingResult {
            case .success(let upload, _, _):
                // Sometimes the server will not return any response JSON. If we don't handle it, the caller will not be able to receive callback.
                //                guard let _ = upload.response else {
                //                    self.handleResponse(0, response: nil, statusCode: StatusCode.none, completion: completion)
                //                    return
                //                }
                
                upload.responseJSON { (dataResponse) in
                    
                    var json: JSON?
                    if let responseVal = dataResponse.result.value {
                        json = JSON(responseVal)
                    }
                    var statusCode: StatusCode = .none
                    if let httpResponse = dataResponse.response, let status = StatusCode(rawValue: httpResponse.statusCode) {
                        statusCode = status
                    }
                    self.handleResponse(99, response: json, statusCode: statusCode, completion: completion)
                    
                }
            case .failure(let encodingError):
                print("Upload failed!")
                print(encodingError)
                self.handleResponse(99, response: nil, statusCode: .none, completion: completion)
            }
        }
        
    }
    
    
    
    func handleResponse(_ attempts: Int, response: JSON?, statusCode: StatusCode, completion: APICallback?) {
        
//        let description = "\(URLString())"
//        NULog.logApiReceived(withFail: !statusCode.success(), withText: description)
        
        if statusCode == .unauthorized && self is SKApi.Auth == false {
            // get current token
            let accessToken = SKApi.accessToken
            guard let refreshToken = accessToken?.refreshToken else { return } // TODO gabriel report error and logout
            
            SKApi.refreshingToken = true
            SKApi.Auth.refreshToken(token: refreshToken).request { (response) in
                SKApi.refreshingToken = false
                
                if response?.success ?? false {
                    self._sendRequest(attempts, completion: completion)
                    SKApi.flushAPICalls()
                } else {
                    // TODO gabriel report error and logout
                }
            }
            
            return
        }
        
        if statusCode.failure() {
            
            if attempts <= 2 {
                let totalAttempts = attempts + 1
                self._sendRequest(totalAttempts, completion: completion)
                
            } else {
                let response = APIResponseType(with: statusCode, success: false, json: response)
                completion?(response)
            }
            return
        }
        
        let success = self.didSucceed(statusCode)
        var response = APIResponseType.init(with: statusCode, success: success, json: response)
        fillResponse(&response)
        completion?(response)
    }
}
