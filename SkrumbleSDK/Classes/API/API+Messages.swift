//
//  API+Chats.swift
//  Alamofire
//
//  Created by Uri Frischman on 2018-06-27.
//

import Foundation
import SwiftyJSON

extension SKApi {
    public enum Messages : APIEndpoint {
        case getMessage(messageId: String, chatId: String)
        case sendText(body : String, chatId : String)
        case reply(message: Message, parentMessage: Message)
        case messages(chatId: String)
        case delete(chatID : String, messageId : Message)
        case createMark(chatId : String, messageId : String, markType: Message.MarkType)
        case deleteMark(chatId : String, messageId : String, markID: String)
        case edit(chatId: String, messageId : String, newBody : String)
        case forward(chatID : String, messageId : String, chats: [String], users: [String])
        
        public func fillResponse(_ response: inout APIResponse<Message>) {
            switch self {
            case .getMessage, .sendText, .reply:
                guard let json = response.json else { return }
                let message = Message(with: json)
                response.value = message
                break
            case .messages:
                guard let json = response.json else {return}
                let messagesJson = json.arrayValue
                var messages : [Message] = []
                for messageJson in messagesJson {
                    let messageObject = Message(with: messageJson)
                    messages.append(messageObject)
                }
                response.array = messages
                break
            case .forward:
                guard let json = response.json else { break }
                guard let msgJson = json.arrayValue.first else { break }
                let message = Message(with: msgJson)
                response.value = message
                break
            default:
                break
            }
        }
        
        public typealias APIResponseType = APIResponse<Message>
        
        public func request(_ completion: ((APIResponse<Message>?) -> Void)?) {
            switch self {
            default:
                _sendRequest(completion: completion)
                break
            }
        }
        
        public var endpoint: String {
            switch self {
            case .sendText(_, let chatId), .messages(let chatId):
                return "/chat/\(chatId)/messages"
            case .getMessage(let messageId,let chatId), .edit(let chatId, let messageId, _):
                return "/chat/\(chatId)/messages/\(messageId)"
            case .reply(let message, _):
                return "/chat/\(message.chat)/messages"
            case .delete(let chatId, let messageId):
                return "/chat/\(chatId)/messages/\(messageId)"
            case .createMark(let chatId, let messageId, _):
                return "/chat/\(chatId)/messages/\(messageId)/thread"
            case .deleteMark(let chatId, let messageId, let markId):
                return "/chat/\(chatId)/messages/\(messageId)/mark/\(markId)"
            case .forward(let chatId, let messageId, _, _):
                return "/chat/\(chatId)/messages/\(messageId)/forward"
            }
        }
        
        public func parameters() -> [String: Any]? {
            var params: [String : Any] = [:]
            switch self {
            case .getMessage:
                params["populate"] = "from,file"
                break
            case .sendText(let body, _):
                params["type"] = Message.MessageType.text.rawValue
                params["body"] = body
            case .reply(let message,_):
                params["type"] = Message.MessageType.text.rawValue
                params["body"] = message.body
            case .messages:
                params["skip"] = 0
                params["populate"] = "from"
                params["limit"] = 100
            case .createMark(_, _, let markType):
                params["type"] = markType.rawValue
            case .edit(_,_,let newBody):
                params["type"] = Message.MessageType.text.rawValue
                params["body"] = newBody
            case .forward(_,_,let chats, let users):
                params["forward_chat"] = chats
                params["users"] = users
            default:
                break
            }
            return params
        }
        
        public func method() -> String {
            switch self {
            case .edit:
                return "PATCH"
            case .sendText, .reply, .createMark, .forward:
                return "POST"
            case .getMessage, .messages:
                return "GET"
            case .delete, .deleteMark:
                return "DELETE"
            }
        }
    }
}

