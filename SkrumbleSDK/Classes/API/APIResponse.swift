//
//  APIResponse.swift
//  SKWebRTC_Example
//
//  Created by Gabriel Hernández on 5/25/18.
//  Copyright © 2018 CocoaPods. All rights reserved.
//

import Foundation
import SwiftyJSON

public protocol APIResponseProtocol: class {
    init(with statusCode: StatusCode, success: Bool, json: JSON?)
}

public class APIResponse<T>: APIResponseProtocol {
    public var success: Bool
    public var statusCode: StatusCode
    public var array: [T]?
    public var value: T?
    var json: JSON?
    
    required public init(with statusCode: StatusCode, success: Bool, json: JSON?) {
        self.success = success
        self.json = json
        self.statusCode = statusCode
    }
}
