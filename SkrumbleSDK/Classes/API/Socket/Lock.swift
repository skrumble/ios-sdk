//
//  Lock.swift
//  NuageTel
//
//  Created by Mu-Sheng Wu on 1/16/17.
//  Copyright © 2017 Skrumble. All rights reserved.
//

import Foundation

class Lock {
    public static func synced(_ lock: AnyObject, closure: () -> ()) {
        objc_sync_enter(lock)
        closure()
        objc_sync_exit(lock)
    }
}
