//
//  GenericObserverProtocol.h
//  NuageTel
//
//  Created by Mu-Sheng Wu on 2017-09-26.
//  Copyright © 2017 Skrumble. All rights reserved.
//

#ifndef GenericObserverProtocol_h
#define GenericObserverProtocol_h

#import <SocketIO/SocketIO-Swift.h>

@protocol GenericObserverProtocol

- (void)onUpdated:(NSString *)verb withEvent:(SocketAnyEvent *)event;

- (void)onAdded:(NSString *)verb withEvent:(SocketAnyEvent *)event;

- (void)onRemoved:(NSString *)verb withEvent:(SocketAnyEvent *)event;

- (void)onUnhandled:(NSString *)verb withEvent:(SocketAnyEvent *)event;

@end

#endif /* GenericObserverProtocol_h */
