//
//  Socket.swift
//  NuageTel
//
//  Created by Mu-Sheng Wu on 1/16/17.
//  Copyright © 2017 Skrumble. All rights reserved.
//

import Foundation
import SocketIO
import SwiftyJSON

extension Notification.Name {
    static let socketRegistered = Notification.Name("Skrumble.SocketRegistered")
}

@objc class Socket: NSObject {
    
    typealias CallBackSocketRequest = ((_ data: Any?, _ status: Int) -> ())?
    
    enum ConnectionEvent: String {
        case Connect = "connect"
        case Disconnect = "disconnect"
        case Error = "error"
        case Reconnect = "reconnect"
        case ReconnectAttempt = "reconnectAttempt"
    }
    
    enum RequestMethod: String {
        case Get = "get"
        case Post = "post"
        case Patch = "patch"
        case Delete = "delete"
    }
    
    // Bridge to be used in objective c class
    @objc class RequestMethodBridge: NSObject {
        class func get() -> NSString { return Socket.RequestMethod.Get.rawValue as NSString }
        class func post() -> NSString { return Socket.RequestMethod.Post.rawValue as NSString }
        class func patch() -> NSString { return Socket.RequestMethod.Patch.rawValue as NSString }
        class func delete() -> NSString { return Socket.RequestMethod.Delete.rawValue as NSString }
    }
    
    enum Verb: String {
        case Updated = "updated"
        case AddedTo = "addedTo"
        case RemovedFrom = "removedFrom"
    }
    
    enum Atttribute: String {
        case Messages = "messages"
        case Users = "users"
        case Favorites = "favorites"
        case Notifications = "notifications"
    }
    
    @objc static let EmitTimeout = 10
    
    private var autoReconnect = true
    
    private var client: SocketIOClient!
    private var connected = false
    private var registered = false
    private var observers: NSPointerArray
    private var reRegTask: DispatchWorkItem?
    
    var completionAfterRegistered : (() -> Void)?
    
    @objc deinit {
//        NULog.logInstanceDealloc("Socket Client");
    }
    
    @objc override init() {
//        NULog.logWebsocket("[INIT] Socket Client")
        self.observers = NSPointerArray.weakObjects()
    }
    
    func connect() -> Void {
        if (client != nil) {
//            NULog.logWebsocket("Connect ignored: already connected")
            return;
        }
        
        //        if (UIApplication.shared.applicationState == .background) {
        //            NULog.logWebsocket("Connect Failed: cannot connect on background")
        //            return;
        //        }
        
        guard let url = URL(string: SKApi.apiConfig.apiHostname) else {
//            NULog.logWebsocket("Connect Failed: cannot get socket server URL")
            return;
        }
        let ssl = url.scheme?.hasPrefix("https") ?? false
        client = SocketIOClient(socketURL: url, config: [.log(false), .secure(ssl), .forceWebsockets(true), .selfSigned(true)])
        
        client.on(clientEvent: .connect, callback: onConnect)
        client.on(clientEvent: .disconnect, callback: onDisconnect)
        client.on(clientEvent: .error, callback: onError)
        client.onAny { [weak self] (event) in
            self?.notifyObservers(event)
        }
        
        
        
//        NULog.logWebsocket("... connection: sent and waiting ...")
        client.connect()
    }
    
    func disconnect() -> Void {
        if (client == nil) {
            // already disconnected
            return;
        }
        
        self.autoReconnect = false
        client.disconnect()
        
        client.off(ConnectionEvent.Connect.rawValue)
        client.off(ConnectionEvent.Disconnect.rawValue)
        client.off(ConnectionEvent.Error.rawValue)
        
        self.observers.clean()
    }
    
    // MARK: --------------------------------------------
    // MARK: Socket Event
    
    @objc func isConnected() -> Bool {
        guard let _ = client else {
            return false
        }
        
        let socketConnectionStatus = client.status
        
        switch socketConnectionStatus {
        case .connected:
            return true
        default:
            return false
        }
        //        return connected
    }
    
    @objc func isRegistered() -> Bool {
        return self.registered
    }
    
    @objc func onConnect(data: [Any], ack: SocketAckEmitter) {
        Lock.synced(self) { connected = true }
        
//        NULog.logWebsocket("socket client CONNECTED !")
        self.sendSocketRegistration()
    }
    
    @objc func onDisconnect(data: [Any], ack: SocketAckEmitter) {
        Lock.synced(self) {
            self.connected = false
            self.registered = false
        }
//        NULog.logWebsocket("socket client DISCONNECTED !")
        
        if (self.autoReconnect) {
            self.reRegister()
        }
    }
    
    @objc func onError(data: [Any], ack: SocketAckEmitter) {
//        NULog.logWebsocket("socket client CONNECTION ERROR !")
        
        self.registered = false
    }
    
    @objc func notifyObservers(_ event: SocketAnyEvent) -> Void {
        Lock.synced(self) {
            var hasBeenProcessed = false
            self.observers.forLoop({ (object: Any?) in
                if (object == nil) { return }
                if let observer: Observer = object as? Observer {
                    let notified = observer.notify(event)
                    if (hasBeenProcessed == false) {
                        hasBeenProcessed = notified
                    }
                }
            })
            if (hasBeenProcessed == false) {
                if (event.event == "reconnectAttempt" || event.event == "statusChange"
                    || event.event == "reconnect" || event.event == "error") {
                    // just ignore those two
                    return
                }
//                NULog.logWebsocket(String.init(format: "IGNORED socket event : %@", event.event))
            }
        }
    }
    
    // MARK: --------------------------------------------
    // MARK: Registration
    
    @objc func sendSocketRegistration() {
        if (SKApi.accessToken == nil) {
//            NULog.logWebsocket("Registration Refused: user is logout.")
            return;
        }
        
//        NULog.logWebsocket("... Registration: sent & waiting registration ...")
        
        let registerEndpoint = "/v3/socket/register"
        
        let request = Socket.createRequest(method: .Post, url: registerEndpoint)
        self.sendApiRequest(request: request) { [weak self] (dict, status) in
            if (self == nil) {
                return;
            }
            
            if (self?.connected == false) {
//                NULog.logWebsocket("... Registration FAILED: not connected to server")
                return;
            }
            
            if (status < 200 || status >= 300) {
//                NULog.logWebsocket(String(format:"Registration FAILED: error server code %d", status))
                
                self?.reRegister()
                return;
            }
            
            self?.reRegTask?.cancel()
            self?.reRegTask = nil
            self?.registered = true
            
            if let completion = self?.completionAfterRegistered {
                completion()
                self?.completionAfterRegistered = nil
            }
//            NULog.logWebsocket("Registration SUCCESS: registetered to skrumble")
            NotificationCenter.default.post(name: .socketRegistered, object: nil)
        }
        
    }
    
    @objc func registerObserver(_ observer: Observer) -> Void {
        Lock.synced(self) {
            self.observers.add(observer)
        }
    }
    
    @objc func unregisterObserver(_ observer: Observer) -> Void {
        Lock.synced(self) {
            self.observers.remove(observer)
        }
    }
    
    @objc func unregisterAllObservers() {
        Lock.synced(self) {
            self.observers.clean()
        }
    }
    
    private func reRegister(after millionseconds: Int = 3000) {
        if reRegTask != nil {
            return
        }
        
        reRegTask = DispatchWorkItem {
            self.sendSocketRegistration()
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(millionseconds), execute: reRegTask!)
    }
    
    @objc func testSocketConnection(request: NSDictionary, callback: CallBackSocketRequest) {
        self.client.emitWithAck("GET", request).timingOut(after: 0.5) { (response) in
            
        }
    }
    
    // MARK: --------------------------------------------
    // MARK: Api Request
    
    @objc func sendApiRequest(request: NSDictionary, timeout: Int = Socket.EmitTimeout, callback: CallBackSocketRequest) {
        let method = request["method"] as? String ?? "GET"
        if (self.client == nil || self.isConnected() == false) {
            self.completionAfterRegistered = { [weak self] () in
                self?.sendApiRequest(request: request, callback: callback)
            }
            self.connect()
            return;
        }
        
        let ackCallback: OnAckCallback = self.client.emitWithAck(method, request)
        
        ackCallback.timingOut(after: Double(timeout)) { (response) in
            
            // response from server
            var data = response[0] as? NSDictionary
            if (data == nil) { data = NSDictionary() }
            let body = data?["body"]
            let status = data?["statusCode"] as? Int ?? 0
            if (callback != nil) {
                callback!(body, status)
            }
        }
    }
    
    @objc static func createRequest(urlRequest: NSMutableURLRequest) -> NSDictionary {
        let url = urlRequest.url?.absoluteString ?? ""
        let stringMethod = urlRequest.httpMethod
        let method = Socket.convertStringRequestMethod(stringMethod: stringMethod)
        var data = NSDictionary()
        
        if let body = urlRequest.httpBody {
            do {
                data = try JSONSerialization.jsonObject(with: body, options: .mutableContainers) as? NSDictionary ?? NSDictionary()
            } catch {
                // TODO
            }
        }

        return createRequest(method: method, url: url, data: data)
    }
    
    static func createRequest(method: RequestMethod, url: String, data: NSDictionary? = nil) -> NSDictionary {
        let authorization = SKApi.accessToken?.getAuthorizationString() ?? ""
        let request: NSMutableDictionary = [
            "headers": ["Authorization": authorization],
            "method": method.rawValue,
            "url": url,
            "data": data ?? [:]
        ]
        
        return request
    }
    
    static func convertStringRequestMethod(stringMethod: String) -> RequestMethod {
        
        var event: Socket.RequestMethod?
        switch stringMethod.lowercased() {
        case Socket.RequestMethod.Get.rawValue: event = Socket.RequestMethod.Get; break
        case Socket.RequestMethod.Post.rawValue: event = Socket.RequestMethod.Post; break
        case Socket.RequestMethod.Patch.rawValue: event = Socket.RequestMethod.Patch; break
        case Socket.RequestMethod.Delete.rawValue: event = Socket.RequestMethod.Delete; break
        default: event = Socket.RequestMethod.Get
        }
        return event!
    }
}

public extension Notification.Name {
    static let socketConnectedAndRegistered = Notification.Name("Skrumble.SocketConnectedAndRegistered")
}
