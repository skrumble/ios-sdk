//
//  AccessToken.swift
//  SKNetwork
//
//  Created by Akash Patel on 2018-05-28.
//  Copyright © 2018 Skrumble. All rights reserved.
//

import Foundation
import SwiftyJSON

public class AccessToken: NSObject {
    
    @objc dynamic var accessToken = ""
    @objc dynamic var tokenType = ""
    @objc dynamic var refreshToken = ""
    @objc dynamic var expireIn: Int = 0
    
    //MARK: Constructor
    
    convenience init(with json: JSON) {
        self.init()
        accessToken = json["access_token"].stringValue
        tokenType = "Bearer"
        refreshToken = json["refresh_token"].stringValue
        expireIn = json["expire_in"].intValue
    }
    
    func getAuthorizationString() -> String{
        
        
        let token = self.accessToken
        
        if token.isEmpty {
            return ""
        }
        
        var tokenType = self.tokenType
        
        if tokenType.isEmpty {
            tokenType = "Bearer"
        }
        
        return self.tokenType + " " + self.accessToken
    }
    
}
