//
//  User.swift
//  SDK_Demo
//
//  Created by 李莉 on 2018/4/27.
//  Copyright © 2018 InfChain. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

open class User: Person {
    
    public enum UserStatus: NSInteger {
        case active = 0
        case  busy = 1
    }
    
    public enum UserState: NSInteger {
        case online = 0
        case offline = 1
    }
    
    //    MARK: user class
    
        /**
         * Email address.
         */
        public var email: String
    
        /**
         * The state of the user, the json are `online` or
         * `offline`
         */
        public var state:UserState?
        
        /**
         * The call availability of this user
         */
        public var status:UserStatus?
    
        /*
         * The user"s password for SIP phone registration.
         */
        public var extensionSecret:String?
    
    
        public var tours:NSArray?
    
        // The user"s preferred interface language.
         // @description
         // Currently this only modifies the interface language of app.skrumble.com, API responses won"t be translated. Supported json are `en` and `es`
        public var language:String = "en"
    
        // The TZ string value of the user"s timezone
    
        public var timezone:String?

        
    
    
        
        // The user"s preferred formatting of time values: `12-hour` or `24-hour`
    
        public var timeformat:String?
        
        // The formatting of dates
    
        public var dateFormat:String?
        
        //`true` if the user has requested UI tooltips for help on app.skrumble.com, `false` if disabled.
    
        public var toolTips:Bool = false
    
        //Devices
    
        public var devices:[Device] = []
    
        // A list of teams this user is on
    
        public var teams:[Team] = []
    

        // The datestamp of when this user was deactivated. Deactivated users cannot send or recieve chat messages or calls, and won"t appear in chat participant lists or lists of users
    
        public var deletedAt:String?
//        public var json: NSDictionary?
    
    
    //    MARK:-
    //    MARK:user init
    
    
    public override init(with json: JSON) {
    

        self.email = json["email"].stringValue


        if json["state"].stringValue.contains("offline") {
            self.state = .offline
        } else {
            self.state = .online
        }
        if json["status"].stringValue.contains("active") {
            self.status = .active
        } else {
            self.status = .busy
        }
        
//        self.plan = json["plan"].stringValue
        self.language = json["language"].stringValue
        self.timezone = json["timezone"].stringValue
        self.timeformat = json["timeformat"].stringValue
        self.dateFormat = json["dateformat"].stringValue
        self.extensionSecret = json["extension_secret"].stringValue
        
        let teamsArr = json["teams"].arrayValue
        
        for teaminfo in teamsArr {
            let teamJson = JSON(teaminfo)
            
            let subTeam = Team.init(with: teamJson)
            self.teams.append(subTeam)
        }
        
        
        if json.dictionary != nil {
            
            if (json.dictionary?.keys.contains("devices"))! {
                let array:NSArray = json["devices"].array! as NSArray
                for deviceinfo in array {
                    let deviceJson = JSON(deviceinfo)
                    let subDevice = Device.init(with: deviceJson)
                    self.devices.append(subDevice)
                }
            }
        }

        self.deletedAt = json["deleted_at"].stringValue
        super.init(with: json)
    }
    
    public convenience init() {
        self.init(with: JSON.null)
    }
    
}
