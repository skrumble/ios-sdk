//
//  Device.swift
//  Skrumble_Example
//
//  Created by 李莉 on 2018/5/18.
//  Copyright © 2018 InfChain. All rights reserved.
//

import UIKit
import SwiftyJSON

open class Device: SKObject {
    // The registration Id of the device
    public var registrationId:String?
    
//    Device's type
    public var type:String?
    
// TODO
    public var debug:String?
    
//    Is this device set "do not disturb" true or false
    public var doNotDisturb:Bool?
    
//    Device's uuid
    public var uuid:String?
    
//    The number of this device's unseen notifications
    public var unseenNotifications:Int?
    
    public override init(with json: JSON) {
        super.init(with: json)
        if json == JSON.null {
            return
        }
        self.registrationId = json["registrationId"].stringValue
        self.type = json["type"].stringValue
        self.debug = json["debug"].stringValue
        self.doNotDisturb = json["doNotDisturb"].bool
        self.uuid = json["uuid"].stringValue
        self.unseenNotifications = json["unseenNotifications"].int
        
    }
}
