//
//  Chat.swift
//  Alamofire
//
//  Created by Uri Frischman on 2018-06-28.
//

import Foundation
import SwiftyJSON

open class Message: SKObject {
    
    public enum MessageType {
        case text, callLog, file, voicemail, participantRemoved, participantAdded
        case chatRenamed, chatPurpose, chatAvatar, chatLocked
        
        public typealias RawValue = String
        
        init(rawValue: RawValue) {
            switch rawValue {
            case "text":
                self = .text
                break
            case "call_log":
                self = .callLog
                break
            case "file":
                self = .file
                break
            case "voicemail":
                self = .voicemail
                break
            case "participant_removed":
                self = .participantRemoved
                break
            case "participant_added":
                self = .participantAdded
                break
            case "chat_renamed":
                self = .chatRenamed
                break
            case "chat_purpose":
                self = .chatPurpose
                break
            case "chat_avatar":
                self = .chatAvatar
                break
            case "chat_locked":
                self = .chatLocked
                break
            default:
                self = .text
                break
            }
        }
        
        public var rawValue: RawValue {
            switch self {
            case .text:
                return "text"
            case .callLog:
                return "call_log"
            case .file:
                return "file"
            case .voicemail:
                return "voicemail"
            case .participantRemoved:
                return "participant_removed"
            case .participantAdded:
                return "participant_added"
            case .chatRenamed:
                return "chat_renamed"
            case .chatPurpose:
                return "chat_purpose"
            case .chatAvatar:
                return "chat_avatar"
            case .chatLocked:
                return "chat_locked"
            }
        }
    }
    
    public enum MarkType: String {
        case pin = "pin"
        case unread = "unread"
    }

    public var type:MessageType?
    
    public var body:String?
    
    public var edited:Bool?
    
    public var deletedAt:String?
    
    public var roomMentions:[Int]?
    
    public var thread:String?
    
    public var chat:String = ""
    
    public var readBy:[String]?
    
    public var from:User?
    
    public var userMentions:[String]?
    
    public var language:String?
    
    public override init(with json:JSON) {
        super.init(with: json)
        
        self.type = MessageType(rawValue: json["type"].stringValue)
        
        self.body = json["body"].stringValue
        self.edited = json["edited"].boolValue
        self.deletedAt = json["deleted_at"].stringValue
        
        var roomMentions : [Int] = []
        let roomMentionsJson = json["room_mentions"].arrayValue
        for roomMention in roomMentionsJson {
            let roomId = roomMention.intValue
            roomMentions.append(roomId)
        }
        
        self.roomMentions = roomMentions
        self.thread = json["thread"].stringValue
        self.chat = json["chat"].stringValue
        var readBys : [String] = []
        let readByJson = json["read_by"].arrayValue
        for readBy in readByJson {
            let readById = readBy.stringValue
            readBys.append(readById)
        }
        
        self.readBy = readBys
        
        let userJson = json["from"]
        
        if userJson.dictionary != nil && userJson.count > 0 {
            self.from = User(with: userJson)
        }
        
        var userMentions : [String] = []
        let userMentionsJson = json["user_mentions"].arrayValue
        for userMention in userMentionsJson {
            let userMentionId = userMention.stringValue
            userMentions.append(userMentionId)
        }
        
        self.userMentions = userMentions
        self.language = json["language"].stringValue
        
    }
    
}
