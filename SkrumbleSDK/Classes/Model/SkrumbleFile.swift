//
//  SkrumbleFile.swift
//  SKNetwork
//
//  Created by Akash Patel on 2018-05-23.
//  Copyright © 2018 Skrumble. All rights reserved.
//

import Foundation
import SwiftyJSON

class SkrumbleFile: SKObject {
    
    @objc dynamic var fileName = ""
    @objc dynamic var url = ""
    @objc dynamic var fileExtension = ""
    @objc dynamic var fileSize = 0 as Double
    
    // Field to ignore for the database
    private var urlData = URL(fileURLWithPath: "")
    private var image: UIImage?
    
    override init() {
        super.init()
    }
    
    
    convenience init(url: URL) {
        self.init()
        self.url = url.absoluteString
        urlData = url
        fileName = url.lastPathComponent
        fileExtension = SkrumbleFile.getExtensionFromFileName(fileName: fileName as NSString)
    }
    
    override init(with json: JSON) {
        super.init(with: json)
        
        fileName = json["filename"].stringValue
        url = json["url"].stringValue
        fileExtension = json["extension"].stringValue
        
        if fileExtension.count == 0 {
            fileExtension = SkrumbleFile.getExtensionFromFileName(fileName: fileName as NSString)
        }
        
        fileSize = json["size"].doubleValue
    }
    
    @objc func setUrlData(url: String) {
        urlData = URL(fileURLWithPath: url)
    }
    
    //MARK: Factory Method
    
    @objc static func createVoiceMessageFile() -> SkrumbleFile{
        let skrumbleFile = SkrumbleFile.init()
        skrumbleFile.createdAt = Date.init()
        skrumbleFile.fileName = "Audio" + String(Int(Date.init().timeIntervalSince1970)) + ".acc"
        skrumbleFile.fileExtension = "acc"
        return skrumbleFile
    }

//MARK: Data Converter
    func getData() -> Data? {
        if (image != nil) {
            if let imgData = image!.jpegData(compressionQuality: 1.0) {
                return imgData
            }
        }
        if let data = try? Data(contentsOf: urlData) {
            return data
        }
        return nil
    }
    
    func getImageData() -> UIImage? {
        if (image != nil) {
            return image
        }
        if let data = getData(), let image = UIImage(data: data) {
            return image
        }
        return nil
    }
    
//MARK: Utility Method
    
    func getMimeType() -> String {
//        return NUDataHelper.mimeType(forPath: url)
        return ""
    }
    
    static func getExtensionFromFileName(fileName: NSString) -> String {
        return fileName.pathExtension
    }
    
    func getSizeStringFormat() -> String {
//        if (fileSize == 0 ) {
//            fileSize = SkrumbleFileManager.getFileSize(skrumbleFile: self)
//        }
        
        var convertedValue: Double = Double(fileSize)
        var multiplyFactor = 0
        let tokens = ["bytes", "KB", "MB", "GB", "TB", "PB",  "EB",  "ZB", "YB"]
        while convertedValue > 1024 {
            convertedValue /= 1024
            multiplyFactor += 1
        }
        return String(format: "%4.2f %@", convertedValue, tokens[multiplyFactor])
    }
    
    func isVoiceMessage() -> Bool {
        return SkrumbleFile.isVoiceMessage(fileName)
    }
    
    @objc static func isVoiceMessage(_ fileName: String) -> Bool {
        let regexString = "Audio(\\d){13}\\.aac"
        let result = fileName.range(of: regexString, options: .regularExpression, range: nil, locale: nil)
        return result != nil
    }
    
    func isTypeAudio() -> Bool {
        let extensionArray = ["wav", "mp3", "amr", "acc"]
        
        if extensionArray.contains(fileExtension.lowercased()) {
            return true
        }
        return false
    }
    
    func isTypePicture() -> Bool {
        let extensionArray = ["png", "jpg", "jpeg", "gif", "heic"]
        
        if extensionArray.contains(fileExtension.lowercased()) {
            return true
        }
        return false
    }
    
    func isTypeVideo() -> Bool {
        let extensionArray = ["wmv", "mp4", "mpg", "m4v", "mov", "avi"]
        
        if extensionArray.contains(fileExtension.lowercased()) {
            return true
        }
        return false
    }
    
    func isTypeDocument() -> Bool {
        let extensionArray = ["pdf", "xls", "doc", "ppt", "xlsx", "docx", "pptx"]
        
        if extensionArray.contains(fileExtension.lowercased()) {
            return true
        }
        return false
    }
}
