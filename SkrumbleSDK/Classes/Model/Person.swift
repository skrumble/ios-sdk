//
//  Person.swift
//  Skrumble_Example
//
//  Created by 李莉 on 2018/5/18.
//  Copyright © 2018 InfChain. All rights reserved.
//

import UIKit
import SwiftyJSON

@objc open class Person: SKObject {

    public var firstName:String
    public var lastName:String
    
    public var color:String?
    public var avatar:String
    
//    The create time of the person
    public var createAt:String?

    override init(with json:JSON) {
        firstName = json["first_name"].stringValue
        lastName = json["last_name"].stringValue
        self.avatar = json["avatar"].stringValue
        super.init(with: json)
    }
    
    func initials() -> String {
        return ""
    }
    
    //    MARK: - TODO
    public static func colorObject() -> UIColor {
        let color = UIColor.init(red: 1, green: 1, blue: 1, alpha: 1)
        return color
    }
    
    public var fullName: String {
        let names = [firstName, lastName]
        return names.joined(separator: " ")
    }
}
