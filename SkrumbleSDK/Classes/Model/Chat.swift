//
//  Chat.swift
//  Alamofire
//
//  Created by Uri Frischman on 2018-06-27.
//

import Foundation
import SwiftyJSON

open class Chat: SKObject {
    
    public enum ChatType: Int {
        case `private`, room;
    }
    
    public var type:ChatType = .private
    
    open var name:String = ""
    
    public var purpose:String?
    
    public var updatedAt:String?
    
    public var favorite:Int?
    
    public var url:String?
    
    public var doNotDisturb:Bool?
    
    public var lastMessageTime:String?
    
    public var roomNumber:Int?
    
    public var callcenterRing:String?
    
    public var callcenterVoicemailPassword:String?
    
    public var callcenterMaxTime:Int?
    
    public var callcenterHoldMusic:String?
    
    public var color:String?
    
    public var lastSeen:String?
    
    public var avatar:String?
    
    public var confPin:Int?
    
    public var messageUpdatedTime:String?
    
    public var pinned:Int?
    
    public var callcenterTransferTo:String?
    
    public var team:String?
    
    public var pin:String?
    
    public var owner:String?
    
    public var users:[User]?
    
    public override init(with json:JSON) {
        super.init(with: json)
        if json == JSON.null {
            return
        }
        
        if json["type"].stringValue.isEqual("private") {
            self.type = ChatType.private
        } else if json["type"].stringValue.isEqual("room") {
            self.type = ChatType.room
        }
        
        self.name = json["name"].stringValue
        self.purpose = json["purpose"].stringValue
        self.updatedAt = json["updated_at"].stringValue
        self.favorite = json["favourite"].intValue
        self.url = json["url"].stringValue
        self.doNotDisturb = json["do_not_disturb"].boolValue
        self.lastMessageTime = json["last_message_time"].stringValue
        self.roomNumber = json["roonNumber"].intValue
        self.callcenterRing = json["callcenter_ring"].stringValue
        self.callcenterVoicemailPassword = json["callcenter_voicemail_password"].stringValue
        self.callcenterMaxTime = json["callcenter_max_time"].intValue
        self.callcenterHoldMusic = json["callcenter_hold_music"].stringValue
        self.color = json["color"].stringValue
        self.lastSeen = json["last_seen"].stringValue
        self.avatar = json["avatar"].stringValue
        self.confPin = json["conf_pin"].intValue
        self.messageUpdatedTime = json["message_updated_time"].stringValue
        self.pinned = json["pinned"].intValue
        self.callcenterTransferTo = json["callcenter_transfer_to"].stringValue
        self.team = json["team"].stringValue
        self.pin = json["pin"].stringValue
        self.owner = json["owner"].stringValue
        let jsonUserArray = json["users"].arrayValue
        var userArray : [User] = []
        for userJson in jsonUserArray {
            let user = User(with: userJson)
            userArray.append(user)
        }
        self.users = userArray
    }
    
}
